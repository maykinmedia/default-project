CREATE USER {{ project_name|lower }};
CREATE DATABASE {{ project_name|lower }} WITH OWNER {{ project_name|lower }};
