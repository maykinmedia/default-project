Who's {{ project_name }}?
=========================================

The {{ project_name }} project was initiated by Maykin.

Maykin currently is the maintainer of the concept API standard, as well as the 
component itself.
